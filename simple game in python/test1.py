import pygame, random, math
from pygame import mixer

#rozpoczęcie 

pygame.init()

#stworzenie ekranu

ekran = pygame.display.set_mode((640, 640))

#tło ekranu

ekranIMG = pygame.image.load('moja_gra/assets/tło1.png')

FPS = 60
clock = pygame.time.Clock()
#muzyka w tle
mixer.music.load('moja_gra/sounds/ugh.wav')
mixer.music.play(-1)
mixer.music.set_volume(0.1)

#efekty dziwękowe
startprzycisk = mixer.Sound('moja_gra/sounds/startgame.wav')
kolizjarybka = mixer.Sound('moja_gra/sounds/rybka.wav')
kolizjaości = mixer.Sound('moja_gra/sounds/ości.wav')
highscoresound = mixer.Sound('moja_gra/sounds/beathighscore2.wav')
koniecsound = mixer.Sound('moja_gra/sounds/koniecgry.wav')

#tytuł, ikona 

pygame.display.set_caption("Breakfast kitty")
icon = pygame.image.load('moja_gra/assets/black-cat.png')
pygame.display.set_icon(icon)

#fonts
small_font = pygame.font.Font("moja_gra/ostrich-regular.ttf", 32)
big_font = pygame.font.SysFont("Avenir Next", 48, True)

dark_green = (107, 144, 128)
light_green = (164, 195, 178)
#menu
def start_menu():
    mixer.music.pause()    
    intro = True
    global running
    while intro == True:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    intro = False
                    running = True
                    startprzycisk.play()
                    mixer.music.unpause()
                if event.key == pygame.K_2:
                    intro = False
                    running = False
            if event.type == pygame.QUIT:
                intro = False
                running = False
            
            menuIMG = pygame.image.load('moja_gra/assets/menutlo.png')
            title = big_font.render("BREAKFAST KITTY", True, dark_green)
            start = small_font.render("Press 1 to start! :)", True, light_green)
            end = small_font.render("Press 2 to quit :(", True, light_green)
            info = small_font.render("Collect fish and beware of the bones!", True, light_green)
            ekran.blit(menuIMG, (0, 0))
            ekran.blit(title, (100, 15))
            ekran.blit(start, (50, 100))
            ekran.blit(end, (420, 100))
            ekran.blit(info, (120, 160))
            pygame.display.update()

                    
#gracz

graczIMG = pygame.image.load('moja_gra/assets/bowl.png')
graczX = 304
graczY = 504
graczX_ruch = 0

def gracz(x, y):
    ekran.blit(graczIMG,(x, y))

#rybka 24px x 24px [boundary prawostronna 616]

rybkaIMG = []
rybkaX = []
rybkaY = []
rybkaY_ruch = []
ilość_rybek = random.randint(4, 6)

for i in range(ilość_rybek):
    rybkaIMG.append(pygame.image.load('moja_gra/assets/fish.png'))
    rybkaX.append(random.randint(0, 616))
    rybkaY.append(random.randint(50, 70))
    rybkaY_ruch.append(1)

def rybka(x, y, i):
    ekran.blit(rybkaIMG[i], (x, y))

#ości 24px x 24px

ościIMG = []
ościX = []
ościY = []
ościY_ruch = []
ilość_ości = random.randint(3, 6)

for j in range(ilość_ości):
    ościIMG.append(pygame.image.load('moja_gra/assets/bones.png'))
    ościX.append(random.randint(0, 616))
    ościY.append(random.randint(50, 70))
    ościY_ruch.append(1)

def ości(x, y, j):
    ekran.blit(ościIMG[j], (x, y))

#punkty 

ilość_punktów = 0

p_textX = 500
p_testY = 15

def pokaz_punkty(x, y):
    punkty = small_font.render("Punkty: " + str(ilość_punktów), True, light_green)
    ekran.blit(punkty, (x, y))

#serca/zycia 
serce = pygame.image.load('moja_gra/assets/heart.png').convert_alpha()
ilość_serc = 3


def zdrowie():
    for i in range(ilość_serc):
        ekran.blit(serce, (i * 50 + 10, 15))


#kolizjarybka 

def czy_kolizja_rybka(rybkaX, rybkaY, graczX, graczY):
    odległość = math.sqrt(math.pow(rybkaX - graczX, 2) + (math.pow(rybkaY - graczY, 2)))
    if odległość < 40:
        kolizjarybka.play()
        return True
    else:
        return False

#kolizjaości

def czy_kolizja_ości(ościX, ościY, graczX, graczY):
    odległość = math.sqrt(math.pow(ościX - graczX, 2) + (math.pow(ościY - graczY, 2)))
    if odległość < 40:
        kolizjarybka.stop()
        kolizjaości.play()
        return True
    else:
        return False

#highscore

def zachowaj_wynik():
    with open('moja_gra/punkty.txt', 'r') as file:
        for i in file:
            str(i)
    high_score = int(i)
    if ilość_punktów > high_score:
        high_score = ilość_punktów
        kolizjarybka.stop()
        kolizjaości.stop()
        highscoresound.play()
    with open('moja_gra/punkty.txt', "w") as file2:
        content = str(high_score)
        file2.write(content)
        file2.close()
        top = small_font.render("Highscore: " +  str(i), True, light_green)
        ekran.blit(top, (500, 40))

#licznik
counter = 0
def licznik():
    global counter
    counter += 1
    if counter == 500:
        counter = 0
        for j in range(ilość_ości):
            for m in range(ilość_ości):
                ościY_ruch[m] += 0.05
        for i in range(ilość_rybek):
            for n in range(ilość_rybek):
                rybkaY_ruch[n] += 0.05

#koniec gry 
def end():
    mixer.music.pause()    
    end = True
    global running
    if ilość_serc == 0:
        koniecsound.play()
        koniec = big_font.render("KONIEC GRY", True, dark_green)            
        ekran.blit(koniec, (190, 300))
        pygame.display.update()
        while end == True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    end = False
                    pygame.quit()


#pętla gry

running = True

start_menu()

while running: 

    clock.tick(FPS)

    #kolor ekranu
    ekran.fill((164, 195, 178))

    #tło ekranu
    ekran.blit(ekranIMG, (0, 0))
            
    #licznik
    licznik()

    #punkty
    pokaz_punkty(p_textX, p_testY)

    #serca
    zdrowie()

    #gracz
    graczX += graczX_ruch

    if graczX <= 0:
        graczX = 0
    if graczX >= 576:
        graczX = 576

    gracz(graczX, graczY)


    #rybka
    for i in range(ilość_rybek):
        rybkaY[i] += rybkaY_ruch[i]

        if rybkaY[i] >= 520:
            rybkaY[i] = random.randint(50, 70)
            rybkaX[i] = random.randint(0, 616)

        #kolizja
    
        kolizja1 = czy_kolizja_rybka(rybkaX[i], rybkaY[i], graczX, graczY)

        if kolizja1:
            ilość_punktów += 1
            rybkaX[i] = random.randint(0, 616)
            rybkaY[i] = 0
        
        rybka(rybkaX[i], rybkaY[i], i)

        for i in range(ilość_rybek):
            if ilość_serc == 0:
                for n in range(ilość_rybek):
                    rybkaY_ruch[n] = 0
                break

    #ości
    for j in range(ilość_ości):

        ościY[j] += ościY_ruch[j]

        if ościY[j] >= 520:
            ościY[j] = random.randint(50, 70)
            ościX[j] = random.randint(0, 616)
        
        #kolizjaości

        kolizja2 = czy_kolizja_ości(ościX[j], ościY[j], graczX, graczY)

        if kolizja2:
            ilość_serc -= 1
            ościX[j] = random.randint(0, 616)
            ościY[j] = random.randint(50, 70)
        
        ości(ościX[j], ościY[j], j)

        for j in range(ilość_ości):
            if ilość_serc == 0:
                for m in range(ilość_ości):
                    ościY_ruch[m] = 0
                end()
                break   


    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False


        #sterowanie 

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                graczX_ruch = -2

            if event.key == pygame.K_RIGHT:
                graczX_ruch = 2

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                graczX_ruch = 0

    zachowaj_wynik()
    pygame.display.update()

    
